<?php

include ("inc/bedding_details.php");

if (isset ($_GET["id"])) {
    $id = $_GET["id"];
    if (isset ($product[$id])) {
     $item = $product[$id];
     }
}
      if (!isset($item)) {
        header("location: index.php");
        exit;
      }

?>


<?php include ("inc/header.php"); ?>
  
  <div class="row">
    <h2 id="bedding_title">Y & L</h2>
  </div class>  
  
  <div id="banner_description"><span></span></div> 
  
  <div class="row">
  
       <div class="row col-xs-2 side_bar">
           <ul class="nav bedding_sidebar">
             <li class="nav-item">
                  <a class="nav-link" href="index.php">Home</a>
             </li>
             <li class="nav-item">
                  <a class="nav-link" href="Bedding.php">Bedding</a>
             </li>
             <li class="nav-item">
                   <a class="nav-link" href="Scarf.php">Scarf</a>
             </li>
             <li class="nav-item">
                   <a class="nav-link" href="Blanket.php">Blanket</a>
            </li>
            <li class="nav-item">
                   <a class="nav-link" href="Decorate.php">Decorate</a>
            </li>
        </ul>
        
        
            <hr>
            
            
        <ul class="nav side_bar_small">    
             <li class="nav-item">
                  <a class="nav-link" href="">Contact</a>
           </li>
           <li class="nav-item">
                  <a class="nav-link" href="">Stores</a>
          </li>
           <li class="nav-item">       
                   <a class="nav-link" href="">Factory</a>
          </li>         
             <li class="nav-item">
                   <a class="nav-link" href="">+ News</a>
            </li>
            
            
        </ul>
       
     </div>
       
       
       
       
  <div class="row col-xs-8">
 
      <div class="row mt-2">
           <div class="col-xs-12 my-2"  id="display_one"> 
                <div class="col-xs-6">
                     <img class="card-img-top img-fluid" src=<?php echo "'".$item["img1"]."'";?>>
                </div>     
                
                 <div class="col-xs-6" id="detail_description">
                <h1><?php echo $item["name"]; ?></h1>
                <p class="my-1">The most stylish</p>
                <p class="mx-2">The highest class</p>
                </div>
           </div>    
       
           <div class="col-xs-12 my-2" id="display_two">  
              <img class="card-img-top img-fluid" src=<?php echo "'".$item["img2"]."'";?>>
             <p class="my-1"></p> 
           </div> 
            
          
     </div>  
 
   </div>
   
   <div class="row col-xs-2">
   </div>
   
   
   
</div>           
        
        <hr>

        
        
<!--Footer-->

    <?php include("inc/footer.php"); ?>

  
<!--footer ends-->

 
  
    

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
  </body>
</html>

?>