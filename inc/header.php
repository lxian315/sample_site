<html>
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Coiny|Lato|Lobster|Nunito|Raleway|Roboto|Tangerine|Dancing+Script|Abril+Fatface" rel="stylesheet">
    
    <title>Young Love || 稚爱</title>
    <link rel="icon" 
      type="image/png" 
      href="images/YoungLove Logo.png" class="img-thumbnail"/>
    

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/bedding_custom.css">
    <link rel="stylesheet" href="css/detail_custom.css">


  </head>

  
  <body class="fade-in">
